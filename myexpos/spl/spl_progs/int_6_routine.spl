[PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 9] = 7;                           // System call number for read

alias userSP R0;                                                                    // This would store the user SP
userSP = SP;

[PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 13] = SP;                         // Save user SP into UPTR
SP = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 11] * 512 - 1;               // and then switch to kernel stack

alias fileDescriptor R1;
fileDescriptor = [[PTBR + 2 * ((userSP - 4) / 512)] * 512 + ((userSP - 4) % 512)];  // Get argument 1 (fd)

if(fileDescriptor == -1) then                                                       // If fd is -1, then do as follows (terminal read)
    alias wordAddr R2;
    wordAddr = [[PTBR + 2 * ((userSP - 3) / 512)] * 512 + ((userSP - 3) % 512)];    // Get argument 2 (word address)

    R3 = wordAddr;                                                                  // R3 would hold the word address
    multipush(R0, R1, R2);                                                          // Push all registers used until now
    R1 = 4;                                                                         // R1 would hold the Terminal Read function number
    R2 = [SYSTEM_STATUS_TABLE + 1];                                                 // R2 would hold the Current PID
    call DEVICE_MANAGER;                                                            // Call the Device Manager module
    multipop(R0, R1, R2);                                                           // Get back all registers that were used

else

    if(fileDescriptor < 0 || fileDescriptor >= 8) then                          // If file descriptor is not in the valid range, do as follows

        [[PTBR + 2 * ((userSP - 1) / 512)] * 512 + (userSP - 1) % 512] = -1;    // Set return value to -1
        [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 9] = 0;               // Set mode flag to 0
        SP = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 13];             // Switch to user stack
        ireturn;                                                                // return to user mode

    endif;

    alias uArea R2;
    uArea = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 11] * 512;        // User Area page starting address

    if([uArea + 2 * fileDescriptor + RESOURCE_TABLE_OFFSET] != FILE) then       // If file descriptor does not correspond to an open file, do as follows

        [[PTBR + 2 * ((userSP - 1) / 512)] * 512 + (userSP - 1) % 512] = -1;    // Set return value to -1
        [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 9] = 0;               // Set mode flag to 0
        SP = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 13];             // Switch to user stack
        ireturn;                                                                // return to user mode

    endif;

    alias openFT R3;
    openFT = [uArea + 2 * fileDescriptor + 1 + RESOURCE_TABLE_OFFSET];          // Get open file table index

    alias inodeIdx R4;
    inodeIdx = [OPEN_FILE_TABLE + 4 * openFT];                                  // Get inode index

    // Acquire Inode
    R5 = inodeIdx;                  // Place inode index into R5
    multipush(R0, R1, R2, R3, R4);  // Push all regs used
    R1 = 4;                         // Acquire Inode function number
    R2 = R5;                        // Place inode index as first argument
    R3 = [SYSTEM_STATUS_TABLE + 1]; // Place current PID as second argument
    call RESOURCE_MANAGER;          // Call Resource Manager module
    R5 = R0;                        // Place return value into R5
    multipop(R0, R1, R2, R3, R4);   // Pop all regs used

    if(R5 == -1) then               // If acquiring inode fails, then do as follows

        [[PTBR + 2 * ((userSP - 1) / 512)] * 512 + (userSP - 1) % 512] = -1;    // Set return value to -1
        [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 9] = 0;               // Set mode flag to 0
        SP = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 13];             // Switch to user stack
        ireturn;                                                                // return to user mode

    endif;

    alias lseekPos R5;
    lseekPos = [OPEN_FILE_TABLE + 4 * openFT + 2];                                  // Get lseek position from open file table entry
    alias wordAddr R6;
    wordAddr = [[PTBR + 2 * ((userSP - 3) / 512)] * 512 + ((userSP - 3) % 512)];    // Get address of place were we want to store the word
    wordAddr = [PTBR + 2 * (wordAddr / 512)] * 512 + (wordAddr % 512);              // Convert to physical address

    if(inodeIdx == INODE_ROOT) then                                                 // If file is root file, then do as follows

        if(lseekPos == 480) then                                                    // If lseek value is 480 (root file size), then do as follows

            // Release Inode
            R7 = inodeIdx;                          // Place inode index value into R7
            multipush(R0);                          // Push R0
            R1 = 5;                                 // Release Inode function number
            R2 = R7;                                // Place inode index as first argument
            R3 = [SYSTEM_STATUS_TABLE + 1];         // Place current PID as second argument
            call RESOURCE_MANAGER;                  // Call Resource Manager module
            multipop(R0);                           // Pop R0

            [[PTBR + 2 * ((userSP - 1) / 512)] * 512 + (userSP - 1) % 512] = -2;    // Set return value to -2
            [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 9] = 0;               // Set mode flag to 0
            SP = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 13];             // Switch to user stack
            ireturn;                                                                // return to user mode

        endif;

        [wordAddr] = [ROOT_FILE + lseekPos];                                        // Get word at lseek pos
        [OPEN_FILE_TABLE + 4 * openFT + 2] = [OPEN_FILE_TABLE + 4 * openFT + 2] + 1;// Increment lseek value by 1 word

    else

        if(lseekPos == [INODE_TABLE + 16 * inodeIdx + 2]) then                      // If lseek value is same as file size, then return -2

            // Release Inode
            R7 = inodeIdx;                          // Place inode index value into R7
            multipush(R0);                          // Push R0
            R1 = 5;                                 // Release Inode function number
            R2 = R7;                                // Place inode index as first argument
            R3 = [SYSTEM_STATUS_TABLE + 1];         // Place current PID as second argument
            call RESOURCE_MANAGER;                  // Call Resource Manager module
            multipop(R0);                           // Pop R0

            [[PTBR + 2 * ((userSP - 1) / 512)] * 512 + (userSP - 1) % 512] = -2;    // Set return value to -2
            [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 9] = 0;               // Set mode flag to 0
            SP = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 13];             // Switch to user stack
            ireturn;                                                                // return to user mode

        endif;

        
        alias blockNo R7;                                   // We would store block number (from where to read) into R7
        blockNo = (lseekPos / 512) + 8;                     // Get field from which we have to get the block number
        blockNo = [INODE_TABLE + 16 * inodeIdx + blockNo];  // Get block number from inode table
        alias offset R8;                                    // We would store the offset into the block here
        offset = lseekPos % 512;                            // Get the offset into the block

        R9 = blockNo;                                       // Place block number into R9
        R10 = offset;                                       // Place block offset into R10
        R11 = wordAddr;                                     // Place word address into R11
        multipush(R0, R3, R4);                              // Push R0 (user SP), R3 (open file table index) and R4 (inode index)
        R1 = 2;                                             // Buffered read function
        R2 = R9;                                            // Place block number as first argument
        R3 = R10;                                           // Place block offset as second argument
        R4 = R11;                                           // Place word address as third argument
        call FILE_MANAGER;                                  // Call File Manager module
        multipop(R0, R3, R4);                               // Pop R0, R3 and R4

        [OPEN_FILE_TABLE + 4 * openFT + 2] = [OPEN_FILE_TABLE + 4 * openFT + 2] + 1;// Increment lseek value by 1 word
        
    endif;

    // Release Inode
    R9 = inodeIdx;                          // Place inode index value into R9
    multipush(R0);                          // Push only R0 because only that is what we need to place return value
    R1 = 5;                                 // Release Inode function number
    R2 = R9;                                // Place inode index as first argument
    R3 = [SYSTEM_STATUS_TABLE + 1];         // Place current PID as second argument
    call RESOURCE_MANAGER;                  // Call Resource Manager module
    multipop(R0);                           // Pop only R0 (since we have placed only R0)

endif;

[[PTBR + 2 * ((userSP - 1) / 512)] * 512 + (userSP - 1) % 512] = 0;    // Set return value to 0 (to indicate success)
[PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 9] = 0;              // Set mode flag to 0
SP = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 13];            // Switch to user stack
ireturn;