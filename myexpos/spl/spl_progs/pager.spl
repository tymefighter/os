alias funcNum R1;
alias PID R2;

if(funcNum == 1) then       // Swap out function

    alias i R3;             // Loop counter
    alias wp R4;            // Would store the PID of a process in wait process and is not swapped out
    alias ws R5;            // Would store the PID of a process in wait semaphore and is not swapped out
    alias highestTick R6;   // Would store the value of the highest tick a non-swapped out process has
    alias htProc R7;        // Would store the PID of a process with highest tick and is not swapped out
    alias stateVal R8;      // This would store the state of the process corresponding to the loop counter
    
    i = 3;                  // Skip idle, init (login) and shell
    wp = -1;                // Initialize all others with -1
    ws = -1;
    highestTick = -1;
    htProc = -1;

    while(i < MAX_PROC_NUM - 1) do                                  // Loop over all the processes (excluding swapper daemon)

        stateVal = [PROCESS_TABLE + 16 * i + 4];

        if(stateVal != TERMINATED &&
            [PROCESS_TABLE + 16 * i + 6] == 0) then                 // If the process is not terminated and not swapped out, then do as follows
            if(stateVal == WAIT_PROCESS) then                       // If process is in wait_process state, then place pid into wp
                wp = i;
                break;                                              // break immediately (highest priority)
            endif;

            if(stateVal == WAIT_SEMAPHORE) then                     // If process is in wait_semaphore, then place pid into ws
                ws = i;
            endif;

            if([PROCESS_TABLE + 16 * i] > highestTick) then             // If process has higher value of tick, then do as follows
                if(stateVal != RUNNING && stateVal != ALLOCATED) then   // If the process is neither in running nor in allocated state, then do as follows
                    highestTick = [PROCESS_TABLE + 16 * i];             // Update highest tick observed
                    htProc = i;                                         // Update process having the highest tick
                endif;
            endif;
        endif;

        i = i + 1;                                                  // Increment loop counter
    endwhile;

    alias proc R9;
    proc = htProc;                                                  // Assign pid of the process with highest tick
    if(ws != -1) then
        proc = ws;                                                  // Assign pid of the process in wait_semaphore
    endif;
    if(wp != -1) then                                               // Assign pid of the process in wait_process
        proc = wp;
    endif;

    if(proc == -1) then                                             // If no process was found, then do as follows
        [SYSTEM_STATUS_TABLE + 5] = 0;                              // Assign paging status to 0, this shows that swapping is not occurring currently
        return;
    endif;

    [PROCESS_TABLE + 16 * proc] = 0;                                // Set tick field to 0
    
    i = 2;                                                          // Loop over the entries of the page table of 'proc' process
    while(i < 10) do

        if([PAGE_TABLE_BASE + 20 * proc + 2 * i] != -1) then        // If page is valid, then do as follows
            
            if((i <= 3 && [MEMORY_FREE_LIST + [PAGE_TABLE_BASE + 20 * proc + 2 * i]] == 1) 
                || (8 <= i && i <= 9)) then                             // If page is a heap page and is not shared, or the page is a user stack page, then do as follows

                multipush(R2, R3, R9);                                  // Push all regs required
                R1 = 6;                                                 // Function number for get swap block
                call MEMORY_MANAGER;                                    // Call Memory Manager Module
                R4 = R0;                                                // Place block number which was returned into R4
                multipop(R2, R3, R9);                                   // Pop all regs pushed

                [DISK_MAP_TABLE + 10 * proc + i] = R4;                  // Place block number corresponding to a process page into the disk map table

                R5 = [PAGE_TABLE_BASE + 20 * proc + 2 * i];            // Place page number of the page to be swapped into R5
                multipush(R2, R3, R5, R9);                              // Push all regs required
                R1 = 1;                                                 // R1 would contain the function number for Disk Store
                R3 = R5;                                                // R3 would contain the page number of the page to be stored in the disk, PID is already present in R2 and block number is already present in R4
                call DEVICE_MANAGER;                                    // Call Device Manger Module
                multipop(R2, R3, R5, R9);                               // Pop all regs pushed

                multipush(R2, R3, R9);                                  // Push all regs required
                R1 = 2;                                                 // Function number for Release Page function
                R2 = R5;                                                // Place the page number of the page to be released as the first argument
                call MEMORY_MANAGER;                                    // Call Memory Manager Module
                multipop(R2, R3, R9);                                   // Pop all regs pushed

                [PAGE_TABLE_BASE + 20 * proc + 2 * i] = -1;             // Invalidate the page table entry
                [PAGE_TABLE_BASE + 20 * proc + 2 * i + 1] = "0000";

            endif;

            if(4 <= i && i <= 7) then                                   // If the page is a code page, then do as follows

                R5 = [PAGE_TABLE_BASE + 20 * proc + 2 * i];             // Place page number of the page to be swapped into R5
                multipush(R2, R3, R9);                                  // Push all regs required
                R1 = 2;                                                 // Function number for Release Page function
                R2 = R5;                                                // Place the page number of the page to be released as the first argument
                call MEMORY_MANAGER;                                    // Call Memory Manager Module
                multipop(R2, R3, R9);                                   // Pop all regs pushed

                [PAGE_TABLE_BASE + 20 * proc + 2 * i] = -1;             // Invalidate the page table entry
                [PAGE_TABLE_BASE + 20 * proc + 2 * i + 1] = "0000";

            endif;

        endif;
        i = i + 1;                                                      // Increment loop counter
    endwhile;

    [PROCESS_TABLE + 16 * proc + 6] = 1;                                // Set swap flag of the process found to 1
    [SYSTEM_STATUS_TABLE + 4] = [SYSTEM_STATUS_TABLE + 4] + 1;          // Increment swap count by 1
    [SYSTEM_STATUS_TABLE + 5] = 0;                                      // Reset paging status to 0
    return;                                                             // Return to caller

endif;

if(funcNum == 2) then       // Swap in function

    alias i R3;             // Loop counter
    alias highestTick R4;   // Would store the value of the highest tick a swapped out process has
    alias proc R5;          // Would store the PID of a process with highest tick and is swapped out

    i = 3;                  // Skip idle, init (login) and shell
    highestTick = -1;       // Initialize all others with -1
    proc = -1;

    while(i < MAX_PROC_NUM - 1) do                                  // Loop over all the processes except swapper daemon

        if([PROCESS_TABLE + 16 * i + 4] == READY 
            && [PROCESS_TABLE + 16 * i + 6] == 1) then              // If process is ready and is swapped out, then do as follows
            if(highestTick < [PROCESS_TABLE + 16 * i]) then         // If tick value is greater than previous maximum, then do as follows
                highestTick = [PROCESS_TABLE + 16 * i];             // Update maximum
                proc = i;                                           // Update process with maximum tick value
            endif;
        endif;

        i = i + 1;                                                  // Increment loop counter
    endwhile;

    if(proc == -1) then                                             // If no process was found, then do as follows
        [SYSTEM_STATUS_TABLE + 5] = 0;                              // Assign paging status to 0, this shows that swapping is not occurring currently
        return;
    endif;

    [PROCESS_TABLE + 16 * proc] = 0;                                // Set tick field to 0

    i = 2;                                                          // Loop over the entries of the page table of 'proc' process
    while(i < 10) do

        if(i == 4) then                                             // Skip over the code pages (i.e. do not load them into the memory)
            i = 7;
        else

            if([DISK_MAP_TABLE + 10 * proc + i] != -1) then         // If disk map table entry is not invalid, then do as follows

                multipush(R2, R3, R5);                              // Push all regs required
                R1 = 1;                                             // Function number for Get Free Page function
                call MEMORY_MANAGER;                                // Call Memory Manager Module
                R6 = R0;                                            // Place free page number into R6
                multipop(R2, R3, R5);                               // Pop all regs pushed

                [PAGE_TABLE_BASE + 20 * proc + 2 * i] = R6;         // Place physical page number into the page table entry of this process
                [PAGE_TABLE_BASE + 20 * proc + 2 * i + 1] = "0110"; // User stack and heap pages are set as unreferenced, valid and writable

                R7 = [DISK_MAP_TABLE + 10 * proc + i];              // Get block number from the disk map table, and place it in R7
                
                multipush(R2, R3, R5, R7);                          // Push all regs required
                R1 = 2;                                             // Function number for Disk Load function, PID is already present in R2 as the first argument
                R3 = R6;                                            // Place page number as second argument
                R4 = R7;                                            // Place block number as third argument
                call DEVICE_MANAGER;                                // Call Device Manager Module
                multipop(R2, R3, R5, R7);                           // Pop all regs pushed

                R6 = PID;                                           // Place PID into R6 for use later
                multipush(R2, R3, R5);                              // Push all regs required
                R1 = 4;                                             // Function number for Release Block function
                R2 = R7;                                            // Place block number as first argument
                R3 = R6;                                            // Place PID as second argument
                call MEMORY_MANAGER;                                // Call Memory Manager Module
                multipop(R2, R3, R5);                               // Pop all regs pushed

                [DISK_MAP_TABLE + 10 * proc + i] = -1;              // Invalidate disk map table entry

            endif;

        endif;

        i = i + 1;                                                      // Increment loop counter
    endwhile;

    [PROCESS_TABLE + 16 * proc + 6] = 0;                                // Set swap flag of the process found to 0
    [SYSTEM_STATUS_TABLE + 4] = [SYSTEM_STATUS_TABLE + 4] - 1;          // Decrement swap count by 1
    [SYSTEM_STATUS_TABLE + 5] = 0;                                      // Reset paging status to 0
    return;                                                             // Return to caller

endif;