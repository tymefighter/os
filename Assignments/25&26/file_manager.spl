alias functionNo R1;                                        // Get function number from R1

if(functionNo == 1 || functionNo == 2) then                 // buffered write or buffered read

    alias blockNo R2;                                       // Disk Block number
    alias offset R3;                                        // Block offset

    // R4 is the word if we are handling the buffered write function,
    // and it is the word address if we are handling the buffered
    // read function

    alias wordAddrword R4;                                  // Word address (physical address) or word itself

    alias bufferNo R5;
    bufferNo = blockNo % 4;                                 // buffer number

    // Acquire Buffer
    R6 = bufferNo;                  // Place buffer number as first argument
    multipush(R1, R2, R3, R4, R5);  // Push all regs used
    R1 = 1;                         // Acquire Buffer function
    R2 = R6;                        // Place buffer number as first argument
    R3 = [SYSTEM_STATUS_TABLE + 1]; // Place PID as second argument
    call RESOURCE_MANAGER;          // Call Resource Manager module
    multipop(R1, R2, R3, R4, R5);   // Pop all regs used

    if([BUFFER_TABLE + 4 * bufferNo] != blockNo) then   // If some other block is present in the buffer cache entry corresponding to 'bufferNo', then do as follows

        if([BUFFER_TABLE + 4 * bufferNo] != -1 && [BUFFER_TABLE + 4 * bufferNo + 1] == 1) then  // If block in cache is valid and dirty, then store it back in the disk

            R6 = 71 + bufferNo;             // Page number of the disk block in the memory (buffer cache)
            R7 = [BUFFER_TABLE + 4 * bufferNo]; // Block number of the disk block where we have to store the dirty block (Very Important)
            multipush(R1, R2, R3, R4, R5);  // Push all regs used
            R1 = 1;                         // Disk Store function
            R2 = [SYSTEM_STATUS_TABLE + 1]; // PID as first argument
            R3 = R6;                        // Page number as second argument
            R4 = R7;                        // Block number as third argument
            call DEVICE_MANAGER;            // Call device manager module
            multipop(R1, R2, R3, R4, R5);   // Pop all regs used

            [BUFFER_TABLE + 4 * bufferNo + 1] = 0;     // Mark the buffer as clean

        endif;

        R6 = 71 + bufferNo;                 // Page number is stored in R6
        R7 = blockNo;                       // Block number is stored in R7
        multipush(R1, R2, R3, R4, R5);      // Push all regs used
        R1 = 2;                             // Disk Load function
        R2 = [SYSTEM_STATUS_TABLE + 1];     // PID as first argument
        R3 = R6;                            // Page number as second argument
        R4 = R7;                            // Block number as third argument
        call DEVICE_MANAGER;                // Call device manager module
        multipop(R1, R2, R3, R4, R5);       // Pop all regs used

        [BUFFER_TABLE + 4 * bufferNo] = blockNo;    // Place block number into buffer cache entry corresponding to buffer number

    endif;

    alias pageNum R6;
    pageNum  = 71 + bufferNo;               // Contains page number of where the block is present in the memory (buffer cache)

    if(functionNo == 1) then
        [pageNum * 512 + offset] = wordAddrword;    // Place the word into the memory buffer copy of the block
        [BUFFER_TABLE + 4 * bufferNo + 1] = 1;      // Mark the buffer as dirty
    else
        [wordAddrword] = [pageNum * 512 + offset];  // Load word at offset in the page to address given by wordAddr
    endif;

    // Release Buffer
    R7 = bufferNo;                          // Place buffer number into R7
    multipush(R1, R2, R3, R4, R5, R6);      // Push all regs used
    R1 = 2;                                 // Release Buffer function number
    R2 = R7;                                // Place buffer number as first argument
    R3 = [SYSTEM_STATUS_TABLE + 1];         // Place PID as second argument
    call RESOURCE_MANAGER;                  // call Resource Manager module
    multipop(R1, R2, R3, R4, R5, R6);       // Pop all regs used

    return;                                 // Return to caller

endif;

if(functionNo == 3) then                                    // open function

    alias filename R2;                                      // filename, passed as argument
    alias i R3;
    i = 0;

    while(i < MAX_FILE_NUM) do                              // Loop over all inode table entries to search for a file with filename given as argument

        if([INODE_TABLE + 16 * i + 1] == filename) then     // If filename named file is found, then break
            break;
        endif;
        i = i + 1;

    endwhile;

    if(i == MAX_FILE_NUM) then                              // If there is no file with name filename, then break

        R0 = -1;                    // Set -1 as return value
        return;                     // return to caller

    endif;
    
    // Acquire Inode
    R4 = i;                         // Place inode index into R4
    multipush(R1, R2, R3);          // Push all regs used
    R1 = 4;                         // Acquire Inode function call
    R2 = R4;                        // Inode index passed as first argument
    R3 = [SYSTEM_STATUS_TABLE + 1]; // PID passed as second argument
    call RESOURCE_MANAGER;          // Call Resource Manager module
    multipop(R1, R2, R3);           // Pop all regs used

    if(R0 == -1) then               // If locking fails, then return -1 (locking failed because inode table entry was invalidated -> i.e. file was deleted)
        return;                     // return to caller
    endif;

    if([INODE_TABLE + 16 * i] == EXEC) then // Only data files can be opened, so if this file is an executable, do as follows

        // Release Inode
        R4 = i;                         // Place inode index into R4
        multipush(R1, R2, R3);          // Push all regs used
        R1 = 5;                         // Release Inode function call
        R2 = R4;                        // Inode index is first argument
        R3 = [SYSTEM_STATUS_TABLE + 1]; // PID is second argument
        call RESOURCE_MANAGER;          // call Resource Manager module
        multipop(R1, R2, R3);           // Pop all regs used

        R0 = -1;                        // Set -1 as the return value
        return;                         // return to caller

    endif;

    alias openFT R4;
    openFT = 0;                         // Open File Table index initialized to 0

    while(openFT < MAX_OPENFILE_NUM) do                 // Loop over the open file table, search for a free entry

        if([OPEN_FILE_TABLE + 4 * openFT] == -1) then   // If a free entry is found, then do as follows
            break;
        endif;
        openFT = openFT + 1;

    endwhile;

    if(openFT == MAX_OPENFILE_NUM) then // If there is no free entry in the open file table, then do as follows

        // Release Inode
        R5 = i;                         // Place inode index into R5
        multipush(R1, R2, R3, R4);      // Push all regs used
        R1 = 5;                         // Release Inode function call number
        R2 = R5;                        // Inode index is first argument
        R3 = [SYSTEM_STATUS_TABLE + 1]; // PID is the second argument
        call RESOURCE_MANAGER;          // Call Resource Manager module
        multipop(R1, R2, R3, R4);       // Pop all regs used

        R0 = -2;                        // Place -2 as return value (no free open file table entry)
        return;                         // return to caller

    endif;

    if(filename == "root") then                             // If filename is 'root', then set inode index to that of root file
        [OPEN_FILE_TABLE + 4 * openFT] = INODE_ROOT;
    else

        if([FILE_STATUS_TABLE + 4 * i + 1] == -1) then      // If File Open Count is -1, then make it 1
            [FILE_STATUS_TABLE + 4 * i + 1] = 1;
        else                                                // Else increment its current value and reassign
            [FILE_STATUS_TABLE + 4 * i + 1] = [FILE_STATUS_TABLE + 4 * i + 1] + 1;
        endif;
    
        [OPEN_FILE_TABLE + 4 * openFT] = i;                 // Set inode index in this entry of the open file table

    endif;

    [OPEN_FILE_TABLE + 4 * openFT + 1] = 1;     // Open instance count is set to 1
    [OPEN_FILE_TABLE + 4 * openFT + 2] = 0;     // Lseek value is set to 0

    // Release Inode
    R5 = i;                         // Place inode index into R5
    multipush(R1, R2, R3, R4);      // Push all regs used
    R1 = 5;                         // Release Inode function call number
    R2 = R5;                        // Inode index is first argument
    R3 = [SYSTEM_STATUS_TABLE + 1]; // PID is the second argument
    call RESOURCE_MANAGER;          // Call Resource Manager module
    multipop(R1, R2, R3, R4);       // Pop all regs used

    R0 = openFT;                    // Return value is the open file table index
    return;                         // return to caller

endif;

if(functionNo == 4) then                                    // Close Function

    alias openFT R2;                                        // Open file table entry index is the first argument
    alias inodeIdx R3;

    inodeIdx = [OPEN_FILE_TABLE + 4 * openFT];              // Get inode index

    [OPEN_FILE_TABLE + 4 * openFT + 1] = [OPEN_FILE_TABLE + 4 * openFT + 1] - 1;    // Decrement Open Instance Count (i.e. share count)

    if([OPEN_FILE_TABLE + 4 * openFT + 1] == 0) then

        alias j R4;                                     // New loop counter
        j = 0;                                          // Set it to 0
        while(j < 3) do                                 // Loop over all fields in the open file table entry
            [OPEN_FILE_TABLE + 4 * openFT + j] = -1;    // Invalidate them all (make all of them -1)
            j = j + 1;
        endwhile;

        if(inodeIdx != INODE_ROOT) then                 // If inode index does not correspond to root file, then do as follows

            [FILE_STATUS_TABLE + 4 * inodeIdx + 1] = [FILE_STATUS_TABLE + 4 * inodeIdx + 1] - 1;    // Decrement file open count (number of open files of a file)
            if([FILE_STATUS_TABLE + 4 * inodeIdx + 1] == 0) then
                [FILE_STATUS_TABLE + 4 * inodeIdx + 1] = -1;                                        // If count reaches 0, make it -1
            endif;

        endif;

    endif;

    return;

endif;