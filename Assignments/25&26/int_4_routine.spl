alias userSP R0;
alias syscallNo R1;
alias retAddr R2;
alias modeAddr R3;
alias uptrAddr R4;

userSP = SP;                                                                    // Get user SP
syscallNo = [[PTBR + 2 * ((userSP - 5) / 512)] * 512 + ((userSP - 5) % 512)];   // Get system call number from the User Stack
retAddr = [PTBR + 2 * ((userSP - 1) / 512)] * 512 + ((userSP - 1) % 512);       // Get Return Address
R10 = PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1];                           // Temporarily Storing for later use
modeAddr = R10 + 9;                                                             // Mode Flag Address
uptrAddr = R10 + 13;                                                            // UPTR Address

[uptrAddr] = SP;                                                                // Place user SP into process table of corresponding process
SP = [R10 + 11] * 512 - 1;                                                      // Switch to kernel stack

if(syscallNo == 1) then                                                         // If create system call is invoked, do as follows

    [modeAddr] = 1;                                                             // Place system call number into mode flag

    alias fileName R5;
    fileName = [[PTBR + 2 * ((userSP - 4) / 512)] * 512 + ((userSP - 4) % 512)];    // Get filename argument
    alias permission R6;
    permission = [[PTBR + 2 * ((userSP - 3) / 512)] * 512 + ((userSP - 3) % 512)];  // Get permission argument
    
    alias i R7;
    i = 0;
    while(i < MAX_FILE_NUM) do                                                      // Loop over the inode table entries to search if a file with the same name is already present
        if([INODE_TABLE + 16 * i + 1] == fileName) then                             // If file is already present, do the following

            [retAddr] = 0;                                                          // Set return value to 0
            [modeAddr] = 0;                                                         // Set mode flag to 0 to indicate user mode
            SP = [uptrAddr];                                                        // Switch to user SP
            ireturn;                                                                // Return to user mode

        endif;

        i = i + 1;
    endwhile;

    i = 0;
    while(i < MAX_FILE_NUM) do                                                      // Loop over the inode table entries to search for a free entry
        if([INODE_TABLE + 16 * i + 1] == -1) then
            break;
        endif;
        i = i + 1;
    endwhile;

    if(i == MAX_FILE_NUM) then                                                  // If there is no free entry in the inode table, then do as follows

        [retAddr] = -1;                                                         // Set return value to -1 because no free entry was found
        [modeAddr] = 0;                                                         // Set mode flag to 0 to indicate user mode
        SP = [uptrAddr];                                                        // Switch to user SP
        ireturn;                                                                // Return to user mode

    endif;

    alias inodeAddr R8;
    inodeAddr = INODE_TABLE + 16 * i;                                           // Placing inode address in a register for later use

    [inodeAddr] = DATA;                     // Set filetype to DATA
    [inodeAddr + 1] = fileName;             // Set filename in the inode table to filename given as argument
    [inodeAddr + 2] = 0;                    // Set filesize to 0

    alias j R9;
    j = 8;
    while(j <= 11) do                       // Loop over all the block entries in the current inode table entry

        [inodeAddr + j] = -1;               // Set all the block numbers in the inode table entry to -1 since no disk blocks have been have been allocated to this file
        j = j + 1;
    endwhile;

    alias userID R9;
    userID = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 3];  // Copy user id from the process table (since this user will own the file now)

    [inodeAddr + 3] = userID;               // Setting userid field in the inode table to that present in the process table corresponding to the current process
    [inodeAddr + 4] = permission;           // Set permission to that given as argument

    alias rootLoc R10;
    rootLoc = ROOT_FILE + 8 * i;

    [rootLoc] = fileName;                                                   // Set filename in the root file to filename given as argument
    [rootLoc + 1] = 0;                                                      // Set filesize to 0
    [rootLoc + 2] = DATA;                                                   // Set filetype to DATA
    [rootLoc + 3] = [USER_TABLE + 2 * userID];                              // Set username to the username from the user table entry corresponding to the index given by userID
    [rootLoc + 4] = permission;                                             // Set permission to the permission passed as argument

    [retAddr] = 0;                                                          // Set return value to 0
    [modeAddr] = 0;                                                         // Set mode flag to 0 to indicate user mode
    SP = [uptrAddr];                                                        // Switch to user SP
    ireturn;                                                                // Return to user mode

endif;

if(syscallNo == 4) then                                                         // If delete system call is invoked, then do as follows

    [modeAddr] = 4;                                                             // Set mode flag to system call number

    alias fileName R5;
    fileName = [[PTBR + 2 * ((userSP - 4) / 512)] * 512 + ((userSP - 4) % 512)];    // Get filename argument

    alias i R6;
    i = 0;

    while(i < MAX_FILE_NUM) do                                                      // Loop over the entries of the inode table to find the index of the file with filename given as argument

        if([INODE_TABLE + 16 * i + 1] == fileName) then                             // If file with given filename is found, then break
            break;
        endif;

        i = i + 1;

    endwhile;

    if(i == MAX_FILE_NUM) then                                                      // If file with given filename is not present, then do as follows

        [retAddr] = 0;                                                              // Set return value to 0
        [modeAddr] = 0;                                                             // Set mode flag to 0 to indicate user mode
        SP = [uptrAddr];                                                            // Switch back to user SP
        ireturn;                                                                    // Return to user mode

    endif;

    alias inodeAddr R7;
    inodeAddr = INODE_TABLE + 16 * i;                                               // Place inode address in R7 for later use

    if([inodeAddr] != DATA) then                                                    // If the file is not a data file, then do as follows

        [retAddr] = -1;                                                             // Set return value to -1 to indicate that file was not deleted (because it was not a data file)
        [modeAddr] = 0;                                                             // Set mode flag to 0 to indicate user mode
        SP = [uptrAddr];                                                            // Switch back to user SP
        ireturn;                                                                    // Return to user mode

    endif;

    if([inodeAddr + 4] == EXCLUSIVE) then                                           // If the permission to this file is EXCLUSIVE, then do as follows

        alias userID R7;
        userID = [PROCESS_TABLE + 16 * [SYSTEM_STATUS_TABLE + 1] + 3];
        if(userID != 1 && userID != [inodeAddr + 3]) then                           // If user is not root (userid = 1) and also not the owner of the file, then do as follows

            [retAddr] = -1;                                                         // Set return value to -1 to indicate that file was not deleted (because the current process did not have permission to do so)
            [modeAddr] = 0;                                                         // Set mode flag to 0 to indicate user mode
            SP = [uptrAddr];                                                        // Switch back to user SP
            ireturn;                                                                // Return to user mode

        endif;

    endif;

    R8 = i;                             // Inode index is stored into R8
    multipush(R0, R1, R2, R3, R4, R5, R6, R7); // Push all regs used
    R1 = 4;                             // Set function number to that of Acquire Inode
    R2 = R8;                            // Place inode index as first argument
    R3 = [SYSTEM_STATUS_TABLE + 1];     // Place PID of current process as second argument
    call RESOURCE_MANAGER;              // Call resource manager module
    multipop(R0, R1, R2, R3, R4, R5, R6, R7);  // Pop all regs used

    if([FILE_STATUS_TABLE + 4 * i + 1] != -1)  then // If file open count field in the file status table entry corresponding to inode index is not -1 (i.e. there are some open instances of this file present in the system)

        // Release the lock
        R8 = i;                             // Place inode index in R8
        multipush(R0, R1, R2, R3, R4, R5, R6, R7); // Push all regs used
        R1 = 5;                             // Set function number to that of Release Inode
        R2 = R8;                            // Place inode index as first argument
        R3 = [SYSTEM_STATUS_TABLE + 1];     // Place current PID as second argument
        call RESOURCE_MANAGER;              // Call Resource Manager module
        multipop(R0, R1, R2, R3, R4, R5, R6, R7);  // Pop all regs used

        [retAddr] = -2;                     // Set return value to -2 to indicate that file was open, hence cannot be deleted
        [modeAddr] = 0;                     // Set mode flag to 0 to indicate user mode
        SP = [uptrAddr];                    // Switch back to user SP
        ireturn;                            // Return to user mode

    endif;

    alias j R8;
    j = 8;
    while(j <= 11) do

        if([inodeAddr + j] != -1) then                                  // If this block is not invalid, do as follows

            R9 = BUFFER_TABLE + 4 * ([inodeAddr + j] % MAX_BUFFER);     // Stored temporarily into R9 since, otherwise we would have to compute it multiple times
            if([R9] == [inodeAddr + j] && [R9 + 1] == 1) then           // If buffer table contains this block and it is dirty, then reset its dirty bit
                [R9 + 1] = 0;                                           // Reset the dirty bit
            endif;

            R9 = [inodeAddr + j];               // Place the block number in R9
            multipush(R0, R1, R2, R3, R4, R5, R6, R7, R8); // Push all regs used
            R1 = 4;                             // Function number for Release Block
            R2 = R9;                            // Place block number as first argument
            R3 = [SYSTEM_STATUS_TABLE + 1];     // Place current PID as second argument
            call MEMORY_MANAGER;                // Call Memory Manager module
            multipop(R0, R1, R2, R3, R4, R5, R6, R7, R8); // Pop all regs used

        endif;
        j = j + 1;

    endwhile;

    [inodeAddr + 1] = -1;                       // Invalidate this Inode Table entry
    [ROOT_FILE + 8 * i] = -1;                   // Invalidate corresponding root file entry

    R8 = i;                                     // Place inode index into R8
    multipush(R0, R1, R2, R3, R4, R5, R6, R7);  // Push all regs used
    R1 = 5;                                     // Function number for Release Inode
    R2 = R8;                                    // Place inode index as second argument
    R3 = [SYSTEM_STATUS_TABLE + 1];             // Place PID as second argument
    call RESOURCE_MANAGER;                      // Call Resource Manager module
    multipop(R0, R1, R2, R3, R4, R5, R6, R7);   // Pop all regs used


    [retAddr] = 0;                              // Set return value to 0 (success)
    [modeAddr] = 0;                             // Set mode flag to 0 to indicate user mode
    SP = [uptrAddr];                            // Switch back to user SP
    ireturn;                                    // Return to user mode

endif;