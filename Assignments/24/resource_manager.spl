alias functionNum R1;

if(functionNum == 1) then                                           // Function to Acquire the buffer

    alias bufferNo R2;                                              // Buffer number is passed as an argument
    alias PID R3;                                                   // PID is passed as an argument

    while([BUFFER_TABLE + 4 * bufferNo + 2] != -1) do               // While buffer is locked by some process, busy wait in this loop

        [PROCESS_TABLE + 16 * PID + 4] = WAIT_BUFFER;               // Set the state to (WAIT_BUFFER, buffer number)
        [PROCESS_TABLE + 16 * PID + 5] = bufferNo;

        multipush(R1, R2, R3);                                      // Push all regs used
        call SCHEDULER;                                             // Call the scheduler to schedule another process
        multipop(R1, R2, R3);                                       // Pop all regs used

    endwhile;

    [BUFFER_TABLE + 4 * bufferNo + 2] = PID;                        // Lock the buffer using the PID passed as argument
    return;

endif;

if(functionNum == 2) then                                           // Function to Release the buffer

    alias bufferNo R2;                                              // Buffer number is passed as an argument
    alias PID R3;                                                   // PID is passed as an argument

    if(PID != [BUFFER_TABLE + 4 * bufferNo + 2]) then               // If argument PID is not equal to the locking PID corresponding to the buffer table entry, do as follows

        R0 = -1;                                                    // Place -1 as return value
        return;                                                     // return to caller

    endif;

    [BUFFER_TABLE + 4 * bufferNo + 2] = -1;                         // Set locking PID to -1 (free the lock)

    alias i R4;
    i = 0;
    while(i < MAX_PROC_NUM) do                                      // Loop over all processes, and do as follows

        if([PROCESS_TABLE + 16 * i + 4] == WAIT_BUFFER &&
            [PROCESS_TABLE + 16 * i + 5] == bufferNo) then          // If any process is waiting for the buffer with buffer number 'bufferNo', then make it Ready
            [PROCESS_TABLE + 16 * i + 4] = READY;
        endif;
        i = i + 1;                                                  // Increment loop counter

    endwhile;

    R0 = 0;                                                         // Place 0 as return value
    return;                                                         // return to caller

endif;

if(functionNum == 3) then                                           // Function to Acquire the disk

    alias currentPID R2;                                            // PID is passed as an argument
    
    while([DISK_STATUS_TABLE] == 1) do                              // While Disk is Busy, do the following

        [PROCESS_TABLE + 16 * currentPID + 4] = WAIT_DISK;          // Set state to WAIT_DISK
        multipush(R1, R2);                                          // Push all registers used
        call SCHEDULER;                                             // Call the scheduler to schedule another process
        multipop(R1, R2);                                           // Pop all registers used

    endwhile;

    [DISK_STATUS_TABLE + 0] = 1;                                    // Set disk status to busy
    [DISK_STATUS_TABLE + 4] = currentPID;                           // Set PID in the Disk Status Table

    return;

endif;

if(functionNum == 4) then                                           // Function to Acquire Inode

    alias inodeIdx R2;                                              // Inode Index is passes as an argument
    alias PID R3;                                                   // PID is passed as an argument

    while([FILE_STATUS_TABLE + 4 * inodeIdx + 0] != -1) do          // If File is locked by some process, then busy wait until it gets unlocked

        [PROCESS_TABLE + 16 * PID + 4] = WAIT_FILE;                 // State is set to (WAIT_FILE, inode index)
        [PROCESS_TABLE + 16 * PID + 5] = inodeIdx;
        
        multipush(R1, R2, R3);                                      // Push all regs used
        call SCHEDULER;                                             // call Scheduler module
        multipop(R1, R2, R3);                                       // Pop all regs used

    endwhile;

    if([INODE_TABLE + 16 * inodeIdx + 1] == -1) then                // If file was invalidated (i.e. deleted) by the time it was acquired, then do as follows

        R0 = -1;                                                    // Place -1 as return value
        return;                                                     // return to caller

    endif;

    [FILE_STATUS_TABLE + 4 * inodeIdx + 0] = PID;                   // Lock the entry corresponding to this file (i.e. corresponding to the inode index)

    R0 = 0;                                                         // Success
    return;                                                         // return to caller

endif;

if(functionNum == 5) then                                           // Function to Release Inode

    alias inodeIdx R2;                                              // Inode index is passed as an argument
    alias PID R3;                                                   // PID is passed as an argument

    if(PID != [FILE_STATUS_TABLE + 4 * inodeIdx + 0]) then          // If PID is not equal to locking PID of this file, then do as follows

        R0 = -1;                                                    // Place -1 as return value
        return;                                                     // return to caller

    endif;

    [FILE_STATUS_TABLE + 4 * inodeIdx + 0] = -1;                    // Free the lock by setting locking PID corresponding to the inode index as -1

    alias i R4;
    i = 1;

    while(i < MAX_PROC_NUM) do                                      // Loop over all the processes and set those processes to READY which were waiting to lock the entry corresponding to inodeIdx

        if([PROCESS_TABLE + 16 * i + 4] == WAIT_FILE &&
            [PROCESS_TABLE + 16 * i + 5] == inodeIdx) then          // If state was (WAIT_FILE, inode index), then do as follows

            [PROCESS_TABLE + 16 * i + 4] = READY;                   // Change the state to READY

        endif;
        i = i + 1;
    endwhile;

    R0 = 0;                                                         // Set return value to 0
    return;                                                         // return to caller

endif;

if(functionNum == 6) then                                           // Function to Acquire a Semaphore

    alias PID R2;                                                   // Takes current PID as an argument
    alias i R3;                                                     // Store the loop counter
    i = 0;
    while(i < MAX_SEM_COUNT) do                                     // Loop to find out a free entry in the Semaphore Table 
        if([SEMAPHORE_TABLE + 4 * i + 1] == 0) then                 // If process count is 0, then break
            break;
        endif;
        i = i + 1;
    endwhile;

    if(i == MAX_SEM_COUNT) then                                     // If there is no free entry, then do as follows,
        R0 = -1;                                                    // place -1 as return value into register R0
        return;                                                     // Return to the caller
    endif;

    [SEMAPHORE_TABLE + 4 * i + 1] = 1;                              // Set process count to 1
    [SEMAPHORE_TABLE + 4 * i + 0] = -1;                             // Set locking PID to -1

    R0 = i;                                                         // Place Semaphore Table Index into R0 as the return value
    return;                                                         // Return to the caller

endif;

if(functionNum == 7) then                                           // Function to Release a Semaphore

    alias semTableIdx R2;                                           // Sem Table Index is passed as the First argument
    alias PID R3;                                                   // PID is passed as the second argument

    if([SEMAPHORE_TABLE + 4 * semTableIdx] == PID) then             // If the Semaphore is locked by the current process, then do as follows

        [SEMAPHORE_TABLE + 4 * semTableIdx] = -1;                   // Set Locking PID to -1 (i.e. unlock the semaphore before release)

        alias i R4;                                                 // Loop counter
        i = 1;
        while(i < MAX_PROC_NUM) do                                  // Loop over all processes
            if([PROCESS_TABLE + 16 * i + 4] == WAIT_SEMAPHORE &&
                [PROCESS_TABLE + 16 * i + 5] == semTableIdx) then   // If ith processes state is (WAIT_SEMAPHORE, sem table index), then make it READY
                [PROCESS_TABLE + 16 * i + 4] = READY;
            endif;

            i = i + 1;
        endwhile;

    endif;

    [SEMAPHORE_TABLE + 4 * semTableIdx + 1] = [SEMAPHORE_TABLE + 4 * semTableIdx + 1] - 1; // Decrement the process count of the semaphore in the semaphore table

    R0 = 0;
    return;                                                         // Return to the caller

endif;


if(functionNum == 8) then                                           // Function to Acquire the terminal

    alias currentPID R2;
    while([TERMINAL_STATUS_TABLE] == 1) do                          // Until the terminal is released, make this process wait in a loop

        [PROCESS_TABLE + 16 * currentPID + 4] = WAIT_TERMINAL;      // Set state of this process to WAIT_TERMINAL
        multipush(R1, R2);                                          // Push all registers used
        call SCHEDULER;                                             // Call scheduler to schedule another process
        multipop(R1, R2);                                           // Pop all registers saved

    endwhile;

    [TERMINAL_STATUS_TABLE] = 1;
    [TERMINAL_STATUS_TABLE + 1] = currentPID;
    return;
endif;

if(functionNum == 9) then                                           // Function to Release the terminal

    alias currentPID R2;
    if(currentPID != [TERMINAL_STATUS_TABLE + 1]) then              // If the process which has not acquired the 
        R0 = -1;                                                    // terminal is trying to release it then return -1
        return;
    endif;

    [TERMINAL_STATUS_TABLE] = 0;                                    // Terminal is now released

    alias i R3;
    i = 1;
    while(i < 16) do

        if([PROCESS_TABLE + 16 * i + 4] == WAIT_TERMINAL) then      // Make all process waiting for terminal to ready
            [PROCESS_TABLE + 16 * i + 4] = READY;
        endif;
        
        i = i + 1;
    endwhile;

    R0 = 0;                                                         // This return value indicates success
    return;
endif;

R0 = -1;                                                            // Function number is other than 8 or 9
return;